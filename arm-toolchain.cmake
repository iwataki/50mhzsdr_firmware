
SET(CMAKE_CROSSCOMPILING TRUE)

set(CROSS_COMPILE "arm-none-eabi-")

SET(CMAKE_C_COMPILER ${CROSS_COMPILE}gcc)
SET(CMAKE_CXX_COMPILER ${CROSS_COMPILE}g++)
SET(CMAKE_C_LINK_EXECUTABLE ${CROSS_COMPILE}g++)
SET(CMAKE_CXX_LINK_EXECUTABLE ${CROSS_COMPILE}g++)

set(ARM_COMPILE_OPTION "-mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${ARM_COMPILE_OPTION} -std=gnu11 -Wa")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ARM_COMPILE_OPTION} -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -Wa")


###linker config
set(CMAKE_EXE_LINKER_FLAGS ""${CMAKE_EXE_LINKER_FLAGS} -T ./linker/STM32F401xC_FLASH.ld -nostartfiles -Xlinker --gc-sections --specs=nano.specs -Wl")
