/*
 * spi.cpp
 *
 *  Created on: 2014/12/10
 *      Author: �@��Y
 */

#include "spi.h"
#include "spiirq.h"
#include <stdio.h>
bool SPI::SPI1_busy=false;
bool SPI::SPI2_busy=false;
bool SPI::SPI3_busy=false;
SPI::SPI(int ch,GPIO_TypeDef*GPIO_SPI,int MISO,int MOSI,int SCK){
	this->GPIOFORSPI=GPIO_SPI;
	this->MISObit=MISO;
	this->MOSIbit=MOSI;
	this->SCKbit=SCK;
	//this->SPIPORT=SPIPortName;
	this->SPI_ch=ch;
	//SPI_InitTypeDef initstr;
	GPIO_InitTypeDef gpioinitstr;
	//GPIO_StructInit(&gpioinitstr);
	gpioinitstr.GPIO_Pin=(0x01<<MOSI)|(0x01<<SCK)|(0x01<<MISO);
	gpioinitstr.GPIO_Mode=GPIO_Mode_AF;
	gpioinitstr.GPIO_PuPd=GPIO_PuPd_UP;
	gpioinitstr.GPIO_OType=GPIO_OType_PP;
	gpioinitstr.GPIO_Speed=GPIO_Speed_25MHz;
	GPIO_Init(GPIO_SPI,&gpioinitstr);
	switch(ch){
	case 1:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1,ENABLE);
		GPIO_PinAFConfig(GPIO_SPI,MISO,GPIO_AF_SPI1);
		GPIO_PinAFConfig(GPIO_SPI,MOSI,GPIO_AF_SPI1);
		GPIO_PinAFConfig(GPIO_SPI,SCK,GPIO_AF_SPI1);
		this->SPIPORT=SPI1;
		break;
	case 2:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);
		GPIO_PinAFConfig(GPIO_SPI,MISO,GPIO_AF_SPI2);
		GPIO_PinAFConfig(GPIO_SPI,MOSI,GPIO_AF_SPI2);
		GPIO_PinAFConfig(GPIO_SPI,SCK,GPIO_AF_SPI2);
		this->SPIPORT=SPI2;
		break;
	case 3:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3,ENABLE);
		GPIO_PinAFConfig(GPIO_SPI,MISO,GPIO_AF_SPI3);
		GPIO_PinAFConfig(GPIO_SPI,MOSI,GPIO_AF_SPI3);
		GPIO_PinAFConfig(GPIO_SPI,SCK,GPIO_AF_SPI3);
		this->SPIPORT=SPI3;
		break;
	}


}
void SPI::config(SPI_MODE mode,SPI_DIR dir,SPI_CLKDIV div,int SPI_Datasize_Bitwidth){
	//printf("mode=%x\n\r",mode);
	SPI_InitTypeDef spiinitstr;
	spiinitstr.SPI_BaudRatePrescaler=div;
	spiinitstr.SPI_CPHA=(uint16_t)(mode&0x0001);
	spiinitstr.SPI_CPOL=(uint16_t)(mode&0x0002);
	spiinitstr.SPI_DataSize=SPI_Datasize_Bitwidth;
	spiinitstr.SPI_Direction=SPI_Direction_2Lines_FullDuplex;
	spiinitstr.SPI_FirstBit=SPI_FirstBit_MSB;
	spiinitstr.SPI_Mode=dir;
	spiinitstr.SPI_NSS=SPI_NSS_Soft;
	SPI_Init(this->SPIPORT,&spiinitstr);
	SPI_NSSInternalSoftwareConfig(this->SPIPORT,SPI_NSSInternalSoft_Set);
	SPI_Cmd(this->SPIPORT,ENABLE);
	while(SPI_I2S_GetFlagStatus(this->SPIPORT,SPI_I2S_FLAG_TXE)==0);
	volatile uint16_t dummy=SPI_I2S_ReceiveData(this->SPIPORT);
}
void SPI::AddHandeler(AbstractHardware*reciever,HardwareCB TxfinishCB,HardwareCB RxfinishCB){
	this->RXfinCB=RxfinishCB;
	this->TxfinCB=TxfinishCB;
	this->reciever=reciever;
	//printf("SPI:TryingAddH\n\r");
	switch(this->SPI_ch){
	case 1:
		SPI1_IRQSet(this);
		break;
	case 2:
		SPI2_IRQSet(this);
		break;
	case 3:
		break;
	}
	if(this->RXfinCB!=0){
		NVIC_InitTypeDef nvicinitstr;
		nvicinitstr.NVIC_IRQChannel=SPI1_IRQn;
		nvicinitstr.NVIC_IRQChannelCmd=ENABLE;
		nvicinitstr.NVIC_IRQChannelPreemptionPriority=3;
		nvicinitstr.NVIC_IRQChannelSubPriority=1;
		NVIC_Init(&nvicinitstr);
		SPI_I2S_ITConfig(this->SPIPORT,SPI_I2S_IT_RXNE,ENABLE);
		//printf("SPI:ITconfOK\n\r");
	}
}
void SPI::Writebyte(unsigned char data){
	SPI_I2S_SendData(this->SPIPORT,data);
}
void SPI::Write(uint16_t data){
	//printf("SEND:%x\n\r",data);
	SPI_I2S_SendData(this->SPIPORT,data);
}
void SPI::IRQ(void){
	uint16_t c=SPI_I2S_ReceiveData(this->SPIPORT);
	//printf("raw=%x\n",c);
	if(this->RXfinCB){
		(reciever->*RXfinCB)(&c,1);
	}
}
int SPI::begin(){
	switch(this->SPI_ch){
	case 1:
		if(this->SPI1_busy){
			return -1;
		}else{
			this->SPI1_busy=1;
		}
		break;
	case 2:
		if(this->SPI2_busy){
			return -1;
		}else{
			this->SPI2_busy=1;
		}
		break;
	case 3:
		if(this->SPI3_busy){
			return -1;
		}else{
			this->SPI3_busy=1;
		}
		break;

	}
	return 1;
}
void SPI::end(){
	switch(this->SPI_ch){
		case 1:
			this->SPI1_busy=0;
			break;
		case 2:
			this->SPI2_busy=0;
			break;
		case 3:
			this->SPI3_busy=0;
			break;

		}
}
