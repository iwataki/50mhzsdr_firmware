/*
 * PWMInputPin.h
 *
 *  Created on: 2014/12/30
 *      Author: 岩滝　宗一郎
 */

#ifndef PWMINPUTPIN_H_
#define PWMINPUTPIN_H_
#include <stm32f4xx_conf.h>
/*! @addtogroup Peripheral */
/*! @{*/

/**
 *
 * @brief PWM信号を読み取る
 */
class PWMInputPin {
private:
	TIM_TypeDef*timer;
	int ch;
	int period;
	unsigned int duty;
	bool SignalOk;
	int Nopulse;
public:
	/**
	 * @brief 初期化
	 * @param Port
	 * @param GPIO_Pin_x
	 * @param Timer
	 * @param IC_ch　[1..4]
	 * @param Period [us]
	 * @param TOP [count value]
	 */
	PWMInputPin(GPIO_TypeDef*Port,int GPIO_Pin_x,TIM_TypeDef*Timer,int IC_ch,int Period);
	/**
	 *	@brief Get duty
	 * @return duty [us];
	 */
	unsigned int Get(void);
	/**
	 * @brief HW interrupt
	 */
	void IRQHandler(void);
	/**
	 * @brief Check input is valid pulse.
	 */
	bool IsValid(void);
};

extern"C"{
void TIM1_CC_IRQHandler(void);
void TIM2_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
void TIM5_IRQHandler(void);
void TIM6_IRQHandler(void);
void TIM7_IRQHandler(void);
void TIM8_CC_IRQHandler(void);
}
/*! @} */
#endif /* PWMINPUTPIN_H_ */
