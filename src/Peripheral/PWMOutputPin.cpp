/*
 * PWMOutputPin.cpp
 *
 *  Created on: 2014/12/30
 *      Author: ���@�@��Y
 */

#include "PWMOutputPin.h"

PWMOutputPin::PWMOutputPin(GPIO_TypeDef*Port,int GPIO_Pin_x,TIM_TypeDef*Timer,int OC_ch,int Period,bool OpenCollector){
	GPIO_InitTypeDef initgpio;
	initgpio.GPIO_Mode=GPIO_Mode_AF;
	if(OpenCollector){
		initgpio.GPIO_OType=GPIO_OType_OD;
	}else{
		initgpio.GPIO_OType=GPIO_OType_PP;
	}
	initgpio.GPIO_Pin=GPIO_Pin_x;
	initgpio.GPIO_Speed=GPIO_High_Speed;
	initgpio.GPIO_PuPd=GPIO_PuPd_NOPULL;
	int pin=0;
	for(pin=0;pin<0x10;pin++){
		if((1<<pin)&GPIO_Pin_x){
			break;
		}
	}
	if(Port==GPIOA){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	}else if(Port==GPIOB){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	}else if(Port==GPIOC){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	}else if(Port==GPIOD){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	}else if(Port==GPIOE){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,ENABLE);
	}else if(Port==GPIOF){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);
	}else if(Port==GPIOG){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG,ENABLE);
	}else if(Port==GPIOH){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);
	}
	int AFflag;
	RCC_ClocksTypeDef clkstatus;
	RCC_GetClocksFreq(&clkstatus);
	int clkfreq;
	if(Timer==TIM1){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
		AFflag=GPIO_AF_TIM1;
		clkfreq=clkstatus.PCLK2_Frequency;
	}else if(Timer==TIM2){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
		AFflag=GPIO_AF_TIM2;
		clkfreq=clkstatus.PCLK1_Frequency;
	}else if(Timer==TIM3){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
		AFflag=GPIO_AF_TIM3;
		clkfreq=clkstatus.PCLK1_Frequency;
	}else if(Timer==TIM4){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
		AFflag=GPIO_AF_TIM4;
		clkfreq=clkstatus.PCLK1_Frequency;
	}else if(Timer==TIM5){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5,ENABLE);
		AFflag=GPIO_AF_TIM5;
		clkfreq=clkstatus.PCLK1_Frequency;
	}else if(Timer==TIM6){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,ENABLE);
	}else if(Timer==TIM7){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7,ENABLE);
	}else if(Timer==TIM8){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8,ENABLE);
		AFflag=GPIO_AF_TIM8;
		clkfreq=clkstatus.PCLK2_Frequency;
	}else if(Timer==TIM9){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9,ENABLE);
		AFflag=GPIO_AF_TIM9;
		clkfreq=clkstatus.PCLK2_Frequency;
	}else if(Timer==TIM10){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10,ENABLE);
		AFflag=GPIO_AF_TIM10;
		clkfreq=clkstatus.PCLK2_Frequency;
	}else if(Timer==TIM11){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11,ENABLE);
		AFflag=GPIO_AF_TIM11;
		clkfreq=clkstatus.PCLK2_Frequency;
	}else if(Timer==TIM12){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12,ENABLE);
		AFflag=GPIO_AF_TIM12;
		clkfreq=clkstatus.PCLK1_Frequency;
	}else if(Timer==TIM13){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM13,ENABLE);
		AFflag=GPIO_AF_TIM13;
		clkfreq=clkstatus.PCLK1_Frequency;
	}else if(Timer==TIM14){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14,ENABLE);
		AFflag=GPIO_AF_TIM14;
		clkfreq=clkstatus.PCLK1_Frequency;
	}
	clkfreq=2*clkfreq;
	GPIO_Init(Port,&initgpio);
	GPIO_PinAFConfig(Port,pin,AFflag);
	TIM_TimeBaseInitTypeDef timebaseinit;
	TIM_OCInitTypeDef OCinit;
	TIM_BDTRInitTypeDef bdtrinit;
	timebaseinit.TIM_ClockDivision=TIM_CKD_DIV1;
	timebaseinit.TIM_CounterMode=TIM_CounterMode_Up;
	timebaseinit.TIM_Period=0xffff;
	timebaseinit.TIM_Prescaler=((clkfreq/1000000)*Period)/0xffff-1;
	timebaseinit.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(Timer,&timebaseinit);
	TIM_Cmd(Timer,ENABLE);
	OCinit.TIM_OCIdleState=TIM_OCIdleState_Reset;
	OCinit.TIM_OCMode=TIM_OCMode_PWM1;
	OCinit.TIM_OCNIdleState=TIM_OCNIdleState_Reset;
	OCinit.TIM_OCNPolarity=TIM_OCNPolarity_Low;
	OCinit.TIM_OCPolarity=TIM_OCPolarity_High;
	OCinit.TIM_OutputNState=TIM_OutputNState_Disable;
	OCinit.TIM_OutputState=TIM_OutputState_Enable;
	OCinit.TIM_Pulse=0;
	switch(OC_ch){
	case 1:
		TIM_OC1PreloadConfig(Timer,TIM_OCPreload_Enable);
		TIM_OC1Init(Timer,&OCinit);
		TIM_CCxCmd(Timer,TIM_Channel_1,TIM_CCx_Enable);

		break;
	case 2:
		TIM_OC2PreloadConfig(Timer,TIM_OCPreload_Enable);
		TIM_OC2Init(Timer,&OCinit);
		TIM_CCxCmd(Timer,TIM_Channel_1,TIM_CCx_Enable);
		break;
	case 3:
		TIM_OC3PreloadConfig(Timer,TIM_OCPreload_Enable);
		TIM_OC3Init(Timer,&OCinit);
		TIM_CCxCmd(Timer,TIM_Channel_1,TIM_CCx_Enable);
		break;
	case 4:
		TIM_OC4PreloadConfig(Timer,TIM_OCPreload_Enable);
		TIM_OC4Init(Timer,&OCinit);
		TIM_CCxCmd(Timer,TIM_Channel_1,TIM_CCx_Enable);
		break;
	}
	this->ch=OC_ch;
	this->timer=Timer;
	this->period=Period;
}
void PWMOutputPin::Set(int pulse_width){
	int duty=0xffff*pulse_width/this->period;
	switch(this->ch){
	case 1:
		TIM_SetCompare1(this->timer,duty);
		break;
	case 2:
		TIM_SetCompare2(this->timer,duty);
		break;
	case 3:
		TIM_SetCompare3(this->timer,duty);
		break;
	case 4:
		TIM_SetCompare4(this->timer,duty);
		break;
	}

}

