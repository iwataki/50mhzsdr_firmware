/*
 * HardwareConf.cpp
 *
 *  Created on: 2014/12/31
 *      Author: 岩滝　宗一郎
 */

#include "HardwareConf.h"
#include <stdio.h>

LSM9DS0*Sensor9Dof;
FullcolorLED*FCIND;

RCServo*servo1;
RCServo*servo2;
RCServo*servo3;
RCServo*servo4;

RCPulseInput*Receiver_CH1;//roll
RCPulseInput*Receiver_CH2;//pitch
RCPulseInput*Receiver_CH3;//thr
RCPulseInput*Receiver_CH4;//yaw

SerialIO*ReceiverExt;//外部レシーバー
/**
 * @addtogroup mini-AHRS
 * @brief デバイスを初期化する
 * つながっているデバイスを開き，初期化を行う
 */
void HAL_init(){
	printf("Open spiport...\n\r");
	SPI*sensorport=new SPI(1,GPIOA,6,7,5);
	DigitalOut*CS_gyro=new DigitalOut(GPIOC,GPIO_Pin_4,RCC_AHB1Periph_GPIOC);
	DigitalOut*CS_xm=new DigitalOut(GPIOC,GPIO_Pin_5,RCC_AHB1Periph_GPIOC);
	printf("SPI and CS pin opened\n\r");
	printf("Creating LSM9DS0\n\r");
	Sensor9Dof=new LSM9DS0(sensorport,CS_gyro,CS_xm);
	printf("LSM9DS0 initialized\n\r");
	printf("Add Device Manager\n\r");
	DeviceManager::Add(Sensor9Dof);
	DigitalOut*LedPinR=new DigitalOut(GPIOC,GPIO_Pin_12,RCC_AHB1Periph_GPIOC);
	DigitalOut*LedPinG=new DigitalOut(GPIOC,GPIO_Pin_11,RCC_AHB1Periph_GPIOC);
	DigitalOut*LedPinB=new DigitalOut(GPIOC,GPIO_Pin_10,RCC_AHB1Periph_GPIOC);
	FCIND=new FullcolorLED(LedPinR,LedPinG,LedPinB,true);
	printf("Add FullcolorLED\n\r");
	DeviceManager::Add(FCIND);
	FCIND->Set(FullcolorLED::Black);
	PWMOutputPin*PB6=new PWMOutputPin(GPIOB,GPIO_Pin_6,TIM4,1,5000,true);//14400
	PWMOutputPin*PB7=new PWMOutputPin(GPIOB,GPIO_Pin_7,TIM4,2,5000,true);
	PWMOutputPin*PB8=new PWMOutputPin(GPIOB,GPIO_Pin_8,TIM4,3,5000,true);
	PWMOutputPin*PB9=new PWMOutputPin(GPIOB,GPIO_Pin_9,TIM4,4,5000,true);
	PB6->Set(1000);
	PB7->Set(1000);
	PB8->Set(1000);
	PB9->Set(1000);
	servo1=new RCServo(PB7,1000,2000);//入力範囲は0.0f-1.0f
	servo2=new RCServo(PB6,1000,2000);
	servo3=new RCServo(PB9,1000,2000);
	servo4=new RCServo(PB8,1000,2000);
	printf("Add RC Servo Output\n\r");
	DeviceManager::Add(servo1);
	DeviceManager::Add(servo2);
	DeviceManager::Add(servo3);
	DeviceManager::Add(servo4);


	PWMInputPin*PA0=new PWMInputPin(GPIOA,GPIO_Pin_0,TIM5,1,14400);
	PWMInputPin*PA1=new PWMInputPin(GPIOA,GPIO_Pin_1,TIM2,2,14400);
	PWMInputPin*PA2=new PWMInputPin(GPIOA,GPIO_Pin_2,TIM5,3,14400);
	PWMInputPin*PA3=new PWMInputPin(GPIOA,GPIO_Pin_3,TIM2,4,14400);
	Receiver_CH1=new RCPulseInput(PA0,0.0,1.0);
	Receiver_CH2=new RCPulseInput(PA1,0.0,1.0);
	Receiver_CH3=new RCPulseInput(PA2,0.0,1.0);
	Receiver_CH4=new RCPulseInput(PA3,0.0,1.0);
	printf("Add Receiver\n\r");
	DeviceManager::Add(Receiver_CH1);
	DeviceManager::Add(Receiver_CH2);
	DeviceManager::Add(Receiver_CH3);
	DeviceManager::Add(Receiver_CH4);

	ReceiverExt=new SerialIO("USART1",16);
	DeviceManager::Add(ReceiverExt);
}
