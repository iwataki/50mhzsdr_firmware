/*
 * StreamIOManager.c
 *
 *  Created on: 2014/12/27
 *      Author: �@��Y
 */

#include "StreamIOManager.h"
#include <string.h>
StreamIOinfo_t*_StreamIOlist[StreamIO_MAXNUM];
int Streamio_num=0;
int StreamIO_registration(StreamIOinfo_t*info){
	int fdnum;
	info->flag=0;
	if(Streamio_num<StreamIO_MAXNUM){
		_StreamIOlist[Streamio_num]=info;
		fdnum=Streamio_num;
		info->fd=fdnum;
		Streamio_num++;
		return fdnum;
	}else{
		return  -1;
	}
}
StreamIOinfo_t*StreamIO_retrival_by_Name(char*Name){
	int i=0;
	for(i=0;i<Streamio_num;i++){
		if(strcmp(Name,_StreamIOlist[i]->name)==0){
			return _StreamIOlist[i];
		}
	}
	return (StreamIOinfo_t*)0;
}
StreamIOinfo_t*StreamIO_retrival_by_Fd(int fd){
	return _StreamIOlist[fd];
}
