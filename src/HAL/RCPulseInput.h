/*
 * RCPulseInput.h
 *
 *  Created on: 2015/01/03
 *      Author: 岩滝　宗一郎
 */

#ifndef RCPULSEINPUT_H_
#define RCPULSEINPUT_H_
#include "AbstractHardware.h"
#include "Peripheral/PWMInputPin.h"
/**
 * @addtogroup HAL
 * @{
 * @brief RC受信機パルス入力デバイスクラス
 *
 */
class RCPulseInput:public AbstractHardware {
	PWMInputPin*InputPin;
	int raw;
	float offset;
	float gain;
public:
	/**
	 * @brief 初期化　PWM入力ピンを指定する．
	 * @param pin
	 * @param Offset
	 * @param Gain
	 */
	RCPulseInput(PWMInputPin*pin,float Offset,float Gain);
	/**
	 * @brief 入力値を返す
	 * @return
	 */
	float Get(void);
	/**
	 * @brief パラメータをアップデート
	 * @param Offset
	 * @param Gain
	 */
	void UpdateParameter(float Offset,float Gain);
	bool IsValid(void);
	void UpdateRequest(void);
	bool IsUpdateFinished(void);
	virtual ~RCPulseInput();
};
/**
 * @}
 */
#endif /* RCPULSEINPUT_H_ */
