/*
 * AbstractHardware.h
 *
 *  Created on: 2014/12/10
 *      Author: 宗一郎
 */

#ifndef ABSTRACTHARDWARE_H_
#define ABSTRACTHARDWARE_H_
#include <stdint.h>
/*! @addtogroup HAL*/
/*! @{*/
/**
 *
 * @brief 抽象デバイスクラス
 */
class AbstractHardware{

public:
/*virtual AbstractHardware(){

}*/
	/**
	 * @brief ハードウェア割り込み等を処理する．下位レイヤーに登録する．
	 * @param buf ハードウェアから帰ってくる値
	 * @param size　そのサイズ
	 */
virtual void HardwareCB(uint16_t*buf,int size){

}
/**
 * @brief デバイスの持っている情報を更新する．
 * 実際の入出力操作はこの関数が呼ばれると実行される．
 */
virtual void UpdateRequest(void){

}
/**
 * @brief アップデートが完了したか調べる．
 *
 * @return アップデート完了=true
 */
virtual bool IsUpdateFinished(void){
	return true;
}
virtual ~AbstractHardware(){
		return;
	}
};
/*! @}*/
#endif /* ABSTRACTHARDWARE_H_ */
