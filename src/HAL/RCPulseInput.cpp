/*
 * RCPulseInput.cpp
 *
 *  Created on: 2015/01/03
 *      Author: ���@�@��Y
 */

#include "RCPulseInput.h"

RCPulseInput::RCPulseInput(PWMInputPin*pin,float Offset,float Gain) {
	// TODO Auto-generated constructor stub
	InputPin=pin;
	this->offset=Offset;
	this->gain=Gain;
	this->raw=0;
}
float RCPulseInput::Get(void){
	return this->raw*gain+offset;
}
bool RCPulseInput::IsValid(void){
	return this->InputPin->IsValid();
}
void RCPulseInput::UpdateRequest(void){
	this->raw=this->InputPin->Get();
}
void RCPulseInput::UpdateParameter(float Offset,float Gain){
	this->offset=Offset;
	this->gain=Gain;
}
bool RCPulseInput::IsUpdateFinished(void){
	return true;
}
RCPulseInput::~RCPulseInput() {
	// TODO Auto-generated destructor stub
}

