/*
 * StreamIOManager.h
 *
 *  Created on: 2014/12/27
 *      Author: 宗一郎
 */

#ifndef STREAMIOMANAGER_H_
#define STREAMIOMANAGER_H_
#ifdef __cplusplus
extern "C"{
#endif
#define StreamIO_MAXNUM 10
/**
 * @addtogroup HAL
 * @return
 */
//! @{
typedef char(*ReadFunc_t)(void);
typedef void(*WriteFunc_t)(char c);
typedef int(*BufftestFunc_t)(void);
/**
 *  @brief ストリーム入出力の管理用構造体
 */
typedef struct{
	int fd;
	int flag;
	char*name;
	ReadFunc_t Readfunction;
	WriteFunc_t Writefunction;
	BufftestFunc_t IsNotEmptyfunc;
} StreamIOinfo_t;
/**
 *  @brief　ストリーム入出力の登録
 *  デバイス開始時に使う
 * @param info
 * @return
 */
int StreamIO_registration(StreamIOinfo_t*info);
/**
 *  @brief　ストリーム情報を名前で取得する
 * @param Name
 * @return
 */
StreamIOinfo_t*StreamIO_retrival_by_Name(char*Name);
/**
 * @brief ストリーム情報をfdで取得　失敗時はnullを返す．
 * @param fd
 * @return
 */
StreamIOinfo_t*StreamIO_retrival_by_Fd(int fd);
#ifdef __cplusplus
}
#endif
//! @}
#endif /* STREAMIOMANAGER_H_ */
