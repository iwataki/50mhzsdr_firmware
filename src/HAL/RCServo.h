/*
 * RCServo.h
 *
 *  Created on: 2014/12/30
 *      Author: 岩滝　宗一郎
 */

#ifndef RCSERVO_H_
#define RCSERVO_H_

#include "AbstractHardware.h"
#include "Peripheral/PWMOutputPin.h"
/**
 * @ingroup HAL
 * @brief RCサーボクラス
 */
class RCServo: public AbstractHardware {
private:
	PWMOutputPin*pin;
	int Centerval;
	int Coeff;
	int duty;
public:
	/**
	 * @brief RCServo初期化
	 *
	 * パルス幅=Centerval+Coeff*角度
	 * @param pin PWMOutputPin
	 * @param Centerval 角度->パルス幅変換係数
	 * @param Coeff 角度->パルス幅変換係数
	 */
	RCServo(PWMOutputPin*pin,int Centerval,int Coeff);
	/**
	 *	@brief パラメータ更新
	 *
	 * @param Centerval　角度->パルス幅変換係数
	 * @param Coeff　角度->パルス幅変換係数
	 */
	void SetParam(int Centerval,int Coeff);
	/**
	 * @brief 角度を設定
	 *
	 * @param deg　角度
	 */
	void Set(float deg);
	void UpdateRequest(void);
	bool IsUpdateFinished(void);
};

#endif /* RCSERVO_H_ */
