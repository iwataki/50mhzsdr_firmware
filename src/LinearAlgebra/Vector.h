/*
 * Vector.h
 *
 *  Created on: 2015/01/10
 *      Author: �@��Y
 */

#ifndef VECTOR_H_
#define VECTOR_H_
#include "Matrix.h"
class Vector {
	friend class Matrix;
private:
	float*Value;
	int dim;
	int*refcount;
public:
	Vector();
	Vector(int dim);
	Vector(const Vector&origin);
	float At(int i)const;
	void At(int i,float val);
	int Dims(void)const;
	const Vector& operator=(const Vector&right);
	void CopyTo(Vector&destination);
	const Vector operator+(const Vector&right)const;
	const Vector operator-(const Vector&right)const;
	float operator*(const Vector&right)const;
	const Vector operator*(const float scalar)const;

	friend const Vector operator*(const float scalar,const Vector&right);
	const Vector operator/(float scalar)const;
	const Vector operator^(const Vector&right)const;
	float Norm(void)const;
	const Vector Normalized(void);
	void Normalize(void);
	void Print(void)const;
	virtual ~Vector();
};

#endif /* VECTOR_H_ */
