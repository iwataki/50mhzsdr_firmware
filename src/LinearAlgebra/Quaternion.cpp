/*
 * Quaternion.cpp
 *
 *  Created on: 2015/01/10
 *      Author: �@��Y
 */

#include "Quaternion.h"
#include <math.h>
Quaternion::Quaternion() {
	// TODO Auto-generated constructor stub
	q0=1.0f;
	q1=0.0f;
	q2=0.0f;
	q3=0.0f;
}
Quaternion::Quaternion(float val[]){
	q0=val[0];
	q1=val[1];
	q2=val[2];
	q3=val[3];
}
Quaternion::Quaternion(float q0,float q1,float q2,float q3){
	this->q0=q0;
	this->q1=q1;
	this->q2=q2;
	this->q3=q3;
}
Quaternion::Quaternion(const Vector&src){
	if(src.Dims()==3){
		q0=0;
		q1=src.At(0);q2=src.At(1);q3=src.At(2);
	}else if(src.Dims()==4){
		q0=src.At(0);q1=src.At(1);q2=src.At(2);q3=src.At(3);
	}else{
		q0=1.0f;
		q1=0.0f;
		q2=0.0f;
		q3=0.0f;
	}
}
float sgn(float f1,float f2){
	if(f1>f2){
		return 1.0f;
	}else{
		return -1.0f;
	}
}
Quaternion::Quaternion(const Matrix&RotationMat){
	float q02,q12,q22,q32;
	float m11,m22,m33;
	m11=RotationMat.At(0,0);
	m22=RotationMat.At(1,1);
	m33=RotationMat.At(2,2);
	q02=0.25f*(m11+m22+m33+1.0f);
	q12=0.25f*(m11-m22-m33+1.0f);
	q22=0.25f*(-m11+m22-m33+1.0f);
	q32=0.25f*(-m11-m22+m33+1.0f);
	if(q02<0.0f)q02=0.0f;
	if(q12<0.0f)q12=0.0f;
	if(q22<0.0f)q22=0.0f;
	if(q32<0.0f)q32=0.0f;
	q0=sqrtf(q02);
	q1=sqrtf(q12)*sgn(RotationMat.At(1,2),RotationMat.At(2,1));
	q2=sqrtf(q22)*sgn(RotationMat.At(2,0),RotationMat.At(0,2));
	q3=sqrtf(q32)*sgn(RotationMat.At(0,1),RotationMat.At(1,0));
}
float Quaternion::GetElem(int n){
	float retval=0.0f;
	switch(n){
	case 0:
		retval=q0;
		break;
	case 1:
		retval=q1;
		break;
	case 2:
		retval=q2;
		break;
	case 3:
		retval=q3;
		break;
	default:
		break;
	}
	return retval;
}
const Vector Quaternion::ToVector4d(void)const{
	Vector result(4);
	result.At(0,q0);result.At(1,q1);result.At(2,q2);result.At(3,q3);
	return result;
}
const Vector Quaternion::Tovector3d(void)const{
	Vector result(3);
	result.At(0,q1);result.At(1,q2);result.At(2,q3);
	return result;
}
void Quaternion::FromVector(Vector&src){
	if(src.Dims()==3){
		q0=0;
		q1=src.At(0);q2=src.At(1);q3=src.At(2);
	}else if(src.Dims()==4){
		q0=src.At(0);q1=src.At(1);q2=src.At(2);q3=src.At(3);
	}else{
		q0=1.0f;
		q1=0.0f;
		q2=0.0f;
		q3=0.0f;
	}
}
const Quaternion Quaternion::Conj(void)const{
	Quaternion result(q0,-q1,-q2,-q3);
	return result;
}
const Quaternion Quaternion::operator*(const Quaternion&right)const{
	float r0,r1,r2,r3;
	float p0=this->q0,p1=this->q1,p2=this->q2,p3=this->q3;
	float q0=right.q0,q1=right.q1,q2=right.q2,q3=right.q3;
	r0=q0*p0-q1*p1-q2*p2-q3*p3;
	r1=q0*p1+q1*p0-q2*p3+q3*p2;
	r2=q0*p2+q1*p3+q2*p0-q3*p1;
	r3=q0*p3-q1*p2+q2*p1+q3*p0;
	Quaternion result(r0,r1,r2,r3);
	return result;
}
const Quaternion Quaternion::operator*(float scalar)const{
	Quaternion result(q0*scalar,q1*scalar,q2*scalar,q3*scalar);
	return result;
}
const Quaternion Quaternion::operator/(float scalar)const{
	Quaternion result(q0/scalar,q1/scalar,q2/scalar,q3/scalar);
	return result;
}
const Quaternion operator*(float scalar,const Quaternion&right){
	Quaternion result(right.q0*scalar,right.q1*scalar,right.q2*scalar,right.q3*scalar);
	return result;
}

const Quaternion Quaternion::operator+(const Quaternion&right)const{
	Quaternion result(this->q0+right.q0,this->q1+right.q1,this->q2+right.q2,this->q3+right.q3);
	return result;
}
const Quaternion Quaternion::operator-(const Quaternion&right)const{
	Quaternion result(this->q0-right.q0,this->q1-right.q1,this->q2-right.q2,this->q3-right.q3);
	return result;
}
void Quaternion::Normalize(void){
	float norm=sqrt(q0*q0+q1*q1+q2*q2+q3*q3);
	if(norm!=0.0f){
		q0=q0/norm;
		q1=q1/norm;
		q2=q2/norm;
		q3=q3/norm;
	}
}
float Quaternion::Norm(void){
	return sqrt(q0*q0+q1*q1+q2*q2+q3*q3);
}

Matrix Quaternion::FindOmegaMatrix(const Vector&omega){
	Matrix result(4,4);
	float omegax=omega.At(0);
	float omegay=omega.At(1);
	float omegaz=omega.At(2);
	result.At(0,0,0.0f);result.At(0,1,-omegax);result.At(0,2,-omegay);result.At(0,3,-omegaz);
	result.At(1,0,omegax);result.At(1,1,0.0f);result.At(1,2,omegaz);result.At(1,3,-omegay);
	result.At(2,0,omegay);result.At(2,1,-omegaz);result.At(2,2,0.0f);result.At(2,3,omegax);
	result.At(3,0,omegaz);result.At(3,1,omegay);result.At(3,2,-omegax);result.At(3,3,0.0f);
	return result;
}
Quaternion::~Quaternion() {
	// TODO Auto-generated destructor stub
}

