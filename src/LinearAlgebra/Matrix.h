/*
 * Matrix.h
 *
 *  Created on: 2015/01/09
 *      Author: �@��Y
 */

#ifndef MATRIX_H_
#define MATRIX_H_
#include "Vector.h"
class Vector;
class Matrix {
private:
	float **Value;
	int row;
	int col;
	int*refcount;
public:
	Matrix(void);
	Matrix(int row,int col);
	/**
	 * �󂢃R�s�[
	 */
	Matrix(const Matrix& origin);
	float At(int row,int col)const;
	void At(int row,int col,float val);
	/**
	 * �󂢃R�s�[
	 * this[i][j]=right[i][j]
	 * return this
	 */
	Matrix& operator=(const Matrix&right);
	/**
	 * �[���R�s�[
	 *
	 */
	void CopyTo(Matrix&destination);
	/**
	 * �s�񓯎m�̉��Z
	 */
	const Matrix operator+(const Matrix&right)const;
	/**
	 *�s�񓯎m�̌��Z
	 */
	const Matrix operator-(const Matrix&right)const;
	/**
	 * �s��̊|�Z
	 */
	const Matrix operator*(const Matrix&right)const;
	/**
	 * �s�񁖃X�J���[
	 */
	const Matrix operator*(float scalar);
	/**
	 *
	 * @param x
	 * @return
	 */
	const Vector operator*(const Vector&x)const;
	/**
	 * �s��ƃX�J���[�̐�
	 */
	friend const Matrix operator*(float scalar,const Matrix&right);
	/**
	 * �]�u
	 */
	const Matrix Transposed(void)const;
	void Transpose(void);
	void Print(void)const;
	virtual ~Matrix();
};
/**
 * �s��ƃX�J���[�̐�
 */
const Matrix operator*(float scalar,const Matrix&right);
#endif /* MATRIX_H_ */
