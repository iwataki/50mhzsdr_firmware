/*
 * Quaternion.h
 *
 *  Created on: 2015/01/10
 *      Author: �@��Y
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_
#include "Vector.h"
#include "Matrix.h"
class Quaternion {
private:
	float q0,q1,q2,q3;
public:
	Quaternion();
	Quaternion(float val[]);
	Quaternion(float q0,float q1,float q2,float q3);
	Quaternion(const Vector&src);
	Quaternion(const Matrix&RotationMat);
	float GetElem(int n);
	const Vector ToVector4d(void)const;
	const Vector Tovector3d(void)const;
	void FromVector(Vector&src);
	const Quaternion Conj(void)const;
	const Quaternion operator*(const Quaternion&right)const;
	const Quaternion operator*(float scalar)const;
	const Quaternion operator/(float scalar)const;
	friend const Quaternion operator*(float scalar,const Quaternion&right);
	const Quaternion operator+(const Quaternion&right)const;
	const Quaternion operator-(const Quaternion&right)const;
	void Normalize(void);
	float Norm(void);
	virtual ~Quaternion();
	static Matrix FindOmegaMatrix(const Vector&omega);
};

#endif /* QUATERNION_H_ */
