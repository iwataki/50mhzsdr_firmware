/*
 * FIRFilter.cpp
 *
 *  Created on: 2015/01/03
 *      Author: ���@�@��Y
 */

#include "FIRFilter.h"

FIRFilter::FIRFilter(float coeff[],int tap){
	this->coefficent=coeff;
	this->delayedvalue=new float[tap-1];
	int i=0;
	for(i=0;i<tap-1;i++){
		this->delayedvalue[i]=0.0f;
	}
	this->tap=tap;
}
float FIRFilter::Execute(float value){
	int i;
	float output=0;
	for(i=tap-3;i>=0;i--){
		this->delayedvalue[i+1]=this->delayedvalue[i];
	}
	for(i=0;i<this->tap-1;i++){
		output+=this->delayedvalue[i]*this->coefficent[i+1];
	}
	output+=value*this->coefficent[0];
	this->delayedvalue[0]=value;
	return output;
}

FIRFilter::~FIRFilter() {
	// TODO Auto-generated destructor stub
	delete[] this->delayedvalue;
}

