/*
 * StateMachine.cpp
 *
 *  Created on: 2015/01/01
 *      Author: ���@�@��Y
 */

#include "StateMachine.h"
#include <stdio.h>
StateMachine::StateMachine() {
	// TODO Auto-generated constructor stub
	this->StateNo_Now=0;
	StateHandlerNextTime=0;
	StateHowMuch=0;
}
int StateMachine::CheckCondition(void){
	return this->StateNo_Now;
}
void StateMachine::Execute(){
	this->StateNo_Now=StateHandlerNextTime(this->StateNo_Now);
	if(this->StateNo_Now>=StateHowMuch){
		printf("Illegal State\n\r");
		return;
	}
	this->StateHandlerNextTime=this->_StateHandlerList[this->StateNo_Now];
}
void StateMachine::AddState(StateHandoler handler){
	this->_StateHandlerList.push_back(handler);
	this->StateHowMuch++;
}
void StateMachine::Reset(){
	StateHandlerNextTime=_StateHandlerList[0];
}
StateMachine::~StateMachine() {
	// TODO Auto-generated destructor stub
	this->_StateHandlerList.clear();
}

