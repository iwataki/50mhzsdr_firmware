/*
 * StateMachine.h
 *
 *  Created on: 2015/01/01
 *      Author: 岩滝　宗一郎
 */

#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_
#include <vector>
typedef int(*StateHandoler)(int);
class StateMachine {
	std::vector<StateHandoler> _StateHandlerList;
	StateHandoler StateHandlerNextTime;
	int StateHowMuch;
	int StateNo_Now;
public:
	StateMachine();
	/**
	 * @brief 現在のステート番号を返す
	 * @return
	 */
	int CheckCondition(void);
	void Execute();
	/**
	 *  @brief 登録した順にステート番号が0から昇順でつく．
	 * @param handler あるステートで実行させたい関数.引数がステート番号，戻り値が次のステート番号
	 */
	void AddState(StateHandoler handler);
	void Reset(void);
	virtual ~StateMachine();
};

#endif /* STATEMACHINE_H_ */
