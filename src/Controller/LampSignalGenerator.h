/*
 * LampSignalGenerator.h
 *
 *  Created on: 2014/12/31
 *      Author: ���@�@��Y
 */

#ifndef LAMPSIGNALGENERATOR_H_
#define LAMPSIGNALGENERATOR_H_

class LampSignalGenerator {
private:
	int ElapsedCycle;
	int DulationCycle;
	float initial;
	float goal;
	bool is_fin;
	bool running;
public:
	LampSignalGenerator();
	void Start(float initial_value,float final_value,int cycle);
	void Reset(void);
	float Execute(void);
	bool IsFinished();
	virtual ~LampSignalGenerator();
};

#endif /* LAMPSIGNALGENERATOR_H_ */
