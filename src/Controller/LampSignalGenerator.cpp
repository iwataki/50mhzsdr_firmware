/*
 * LampSignalGenerator.cpp
 *
 *  Created on: 2014/12/31
 *      Author: ���@�@��Y
 */

#include "LampSignalGenerator.h"

LampSignalGenerator::LampSignalGenerator() {
	// TODO Auto-generated constructor stub
	this->DulationCycle=0;
	this->ElapsedCycle=0;
	this->goal=0;
	this->initial=0;
	this->is_fin=true;
	this->running=false;
}
void LampSignalGenerator::Start(float initial_value,float final_value,int cycle){
	this->initial=initial_value;
	this->goal=final_value;
	if(!running){
		this->ElapsedCycle=0;
		is_fin=false;
		running=true;
	}
	this->DulationCycle=cycle;
}
void LampSignalGenerator::Reset(void){
	this->ElapsedCycle=0;
	this->is_fin=true;
	this->running=false;
}
float LampSignalGenerator::Execute(void){
	float retval=this->goal;
	if(this->DulationCycle!=0){
		if(this->DulationCycle>this->ElapsedCycle){
			retval=this->initial+(this->goal-this->initial)*(this->ElapsedCycle*1.0f/this->DulationCycle);
			this->ElapsedCycle++;
			is_fin=false;
		}else{
			is_fin=true;

		}
	}
	return retval;
}
bool LampSignalGenerator::IsFinished(void){
	return this->is_fin;
}
LampSignalGenerator::~LampSignalGenerator() {
	// TODO Auto-generated destructor stub
}

