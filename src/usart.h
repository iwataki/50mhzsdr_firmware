/*
 * uart.h
 *
 *  Created on: 2012/09/28
 *      Author: 岩滝　宗一郎
 */

#ifndef UART_H_
#define UART_H_
#include "stdint.h"
#include "HAL/StreamIOManager.h"
enum uart_brr{
	br2400=0x3a98,  //!< br2400
	br9600=0x0ea6,  //!< br9600
	br19200=0x0753, //!< br19200
	br57600=0x0271, //!< br57600
	br115200=0x138, //!< br115200
	br230400=0x09c, //!< br230400
	br460800=0x04e, //!< br460800
	br921600=0x027, //!< br921600
	br2250000=0x010,//!< br2250000
	br4600000=0x008,//36MHzでは不可
};//ビットレート
#define BUFF_SIZE 1024//2^xsize
typedef struct ringbuff{
	int write_p,read_p,state;
	char data[BUFF_SIZE];
} ringbuff_t;//ソフトウエアリングバッファ
typedef struct ringbuff_dma{
	volatile int read_p;
	uint16_t data[BUFF_SIZE];
} ringbuf_dma_t;//dmaによる受信バッファ
#ifdef __cplusplus
extern "C"{
#endif
void init_usart(int ch,uint32_t uart_brr);
void usart1_putc(char c);
char usart1_getc_noblock(void);
int usart1_is_received(void);
char usart1_getc(void);
void usart2_putc(char c);
char usart2_getc_noblock(void);
int usart2_is_received(void);
char usart2_getc(void);
void usart6_putc(char c);
char usart6_getc_noblock(void);
int usart6_is_received(void);
char usart6_getc(void);
#ifdef __cplusplus
}
#endif

class Usart{
private:
	int ch;
	StreamIOinfo_t info;
public:
	/**
	 * @brief USART初期化関数
	 *
	 * @param ch USARTのチャンネル[1,2,6]
	 * @param uart_brr ボーレート
	 */
	Usart(int ch,uint32_t uart_brr);
	/**
	 * @brief 一文字出力
	 *
	 * @param 文字
	 */
	void putc(unsigned char);
	/**
	 *　@brief 一文字受信（ブロッキング）
	 *
	 * @return 受信した文字
	 */
	char getc(void);
	/**
	 * @brief バッファを出力（未実装）
	 *
	 * @param buf
	 * @param count
	 * @return
	 */
	int write(char*buf,int count);
	/**
	 *	@brief バッファへ入力（未実装）
	 *
	 * @param buf
	 * @param count
	 * @return
	 */
	int read(char*buf,int count);
};
#endif /* UART_H_ */
