/**
 * @file HardwareConf.h
 *
 *  @date 2014/12/31
 *   @author 岩滝　宗一郎
 *
 *   @brief HALの設定
 */
#ifndef HARDWARECONF_H_
#define HARDWARECONF_H_
///使うHALのインクルードファイルを書く
#include "HAL/FullcolorLED.h"
#include "HAL/LSM9DS0.h"
#include "HAL/RCServo.h"
#include "HAL/RCPulseInput.h"
#include "HAL/SerialIO.h"
#include "Internal/DeviceManager.h"

//! 使用するデバイス
extern LSM9DS0*Sensor9Dof;
extern FullcolorLED*FCIND;
extern RCServo*servo1;
extern RCServo*servo2;
extern RCServo*servo3;
extern RCServo*servo4;
extern RCPulseInput*Receiver_CH1;
extern RCPulseInput*Receiver_CH2;
extern RCPulseInput*Receiver_CH3;
extern RCPulseInput*Receiver_CH4;

extern SerialIO*ReceiverExt;
/**
 * @ingroup mini-AHRS
 * @brief HALの初期化関数，制御ループを開始する前に呼ぶ
 */
void HAL_init(void);




#endif /* HARDWARECONF_H_ */
