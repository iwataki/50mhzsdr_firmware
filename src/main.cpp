/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/main.c 
  * @author  MCD Application Team
  * @version V1.4.0
  * @date    04-August-2014
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "Peripheral/gpio.h"
#include "HAL/LSM9DS0.h"
#include "Peripheral/spi.h"
#include "HAL/FullcolorLED.h"
#include "Peripheral/PWMOutputPin.h"
#include "Internal/DeviceManager.h"
#include "Internal/TimingManager.h"
#include "HardwareConf.h"
#include "hwerror.h"
#include "controlapp.h"
#include <vector>
#include<stdlib.h>
#include <stdio.h>
#include <math.h>
/** @addtogroup mini-AHRS
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t uwTimingDelay;
//RCC_ClocksTypeDef RCC_Clocks;

/* Private function prototypes -----------------------------------------------*/
static void Delay(__IO uint32_t nTime);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
extern"C"
void loop(void);
FILE*Xbeefile;
int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       files (startup_stm32f40_41xxx.s/startup_stm32f427_437xx.s/
       startup_stm32f429_439xx.s/startup_stm32f401xx.s or startup_stm32f411xe.s)
       before to branch to application main.
       To reconfigure the default setting of SystemInit() function, 
       refer to system_stm32f4xx.c file */

  /* SysTick end of count event each 1ms */
  //RCC_GetClocksFreq(&RCC_Clocks);
  //SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);
//  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
//  GPIO_InitTypeDef GPIO_Initstruct;
//  GPIO_Initstruct.GPIO_Mode=GPIO_Mode_OUT;
//  GPIO_Initstruct.GPIO_OType=GPIO_OType_PP;
//  GPIO_Initstruct.GPIO_Pin=GPIO_Pin_11;
//  GPIO_Initstruct.GPIO_PuPd=GPIO_PuPd_NOPULL;
//  GPIO_Initstruct.GPIO_Speed=GPIO_Medium_Speed;
//  GPIO_Init(GPIOC,&GPIO_Initstruct);
  //Usart usarta(6,38400);
	NVIC_SetVectorTable(NVIC_VectTab_FLASH,0x000);//!<DFUファーム分，割り込みベクタを移動
	NVIC_SetPriorityGrouping(NVIC_PriorityGroup_3);//!<割り込み初期化
  Usart usarta(1,115200);//!<USARTを開く
  Usart Xbee(6,38400);
  //usarta.putc('A');
  freopen("USART1","r",stdin);//!<scanfの入力先をUSART1に設定
  freopen("USART1","w",stdout);//!<printfの出力先をUSART1に設定
  Xbeefile=fopen("USART6","r+");
  printf("mini-AHRS\n\r");
  printf("Build:%s %s\n\r",__DATE__,__TIME__);
  //char*str=(char*)malloc(3*sizeof(char));
  //str="ab";
  //free(str);
  /* Add your application code here */
  /* Insert 50 ms delay */
  //Delay(50);

  //DigitalOut CS_gyro(GPIOC,GPIO_Pin_4,RCC_AHB1Periph_GPIOC);
  //DigitalOut CS_xm(GPIOC,GPIO_Pin_5,RCC_AHB1Periph_GPIOC);
  //SPI spiport4sensor(1,GPIOA,6,7,5);
  //LSM9DS0 sensor(&spiport4sensor,&CS_gyro,&CS_xm);
//  DigitalOut LedPinR(GPIOC,GPIO_Pin_12,RCC_AHB1Periph_GPIOC);
//  DigitalOut LedPinG(GPIOC,GPIO_Pin_11,RCC_AHB1Periph_GPIOC);
//  DigitalOut LedPinB(GPIOC,GPIO_Pin_10,RCC_AHB1Periph_GPIOC);
//  FullcolorLED FCIND(&LedPinR,&LedPinG,&LedPinB,true);
//  PWMOutputPin PB6(GPIOB,GPIO_Pin_6,TIM4,1,14400,0xffff);
//  PWMOutputPin PB7(GPIOB,GPIO_Pin_7,TIM4,2,14400,0xffff);
//  PWMOutputPin PB8(GPIOB,GPIO_Pin_8,TIM4,3,14400,0xffff);
//  PWMOutputPin PB9(GPIOB,GPIO_Pin_9,TIM4,4,14400,0xffff);
//  PB6.Set(1500);
//  PB7.Set(1500);
//  PB8.Set(1500);
//  PB9.Set(1500);
//  DeviceManager::Add(&FCIND);
//  DeviceManager::Add(&sensor);
 // volatile float x=0.0;
  //std::vector<float> v;
 // v.push_back(0.1);
  //v.push_back(0.2);
  //float v[2]={1.1,2.0};
 // testclass t1;
 // t1.set_x(3);
 // t1.set_y(2);
 // int y=t1.getxy();
  HAL_init();//!デバイスを初期化
  printf("Hal_init_ok\n\r");
  ControlInit();
  /* Infinite loop */
  int i=0;
  //int colorflag=0;
  //int16_t Gyro[3];
  //int16_t Acc[3];
  //int16_t Mag[3];
  //char c=0xf0;
  //printf("c=%x\n\r",c);
  //scanf("%d",&i);
  printf("\x1b[2J");//CLS
  printf("\x1b[>5h");//Cursor invisible
  //float testv=0.0f;
  TimingManager::Init(5,loop);//!<制御用周期関数loop()を開始
  while (1)
  {
	  //printf("\033[H");//Goto (0,0)
	  //printf("Wait Update\n\r");
	  //sensor.UpdateRequest();
	  //FCIND.UpdateRequest();
	  //while(sensor.IsUpdateFinished()==false);
	  //printf("\033[H");
	 // printf("            \n\r");

//	  c=usarta.getc();
//	  if('A'<=c&&c<='Z'){
//		  c=c-'A'+'a';
//	  }else if('a'<=c&&c<='z'){
//		  c=c-'a'+'A';
//	  }
//	  if(c!=0){
//		  usarta.putc(c);
//		  //usarta.putc('B');
//	  }
	  //x=v[0]*x+0.01f*(y)*v[1];
	  //testv+=0.05f;
	  //printf("float test %d\n\r",(int)testv);


	  //FCIND.Set((FullcolorLED::Color)colorflag);
	  //colorflag++;
	  //colorflag&=0x07;
	  Delay(50);
  }
}
/**
 * @brief 制御ループ関数
 */
void loop(void){
	/*static int colorflag=0;
	static float pos=0.0;
	int16_t Gyro[3];
	int16_t Acc[3];
	int16_t Mag[3];
	Sensor9Dof->ReadRaw(Gyro,Acc,Mag);//!ジャイロモジュールから値を取る
	FCIND->Set((FullcolorLED::Color)colorflag);//!フルカラーLEDに色を設定
	printf("\033[H");
	printf("Gyro:\n\r");
	printf("X:%10d,Y:%10d,Z:%10d\n\r",Gyro[0],Gyro[1],Gyro[2]);
	printf("Acc:\n\r");
	printf("X:%10d,Y:%10d,Z:%10d\n\r",Acc[0],Acc[1],Acc[2]);
	printf("Mag:\n\r");
	printf("X:%10d,Y:%10d,Z:%10d\n\r",Mag[0],Mag[1],Mag[2]);
	printf("Ch1:%6d,Ch2:%6d,Ch3:%6d,Ch4:%6d\n\r",(int)Receiver_CH1->Get(),(int)Receiver_CH2->Get(),(int)Receiver_CH3->Get(),(int)Receiver_CH4->Get());
	float roll;
	float pitch;
	roll=asinf(Acc[0]/sqrtf(Acc[0]*Acc[0]+Acc[1]*Acc[1]+Acc[2]*Acc[2]))*1000;
	pitch=asinf(Acc[1]/sqrtf(Acc[0]*Acc[0]+Acc[1]*Acc[1]+Acc[2]*Acc[2]))*1000;
	float servoval=45.0f+pos;
	servo1->Set(servoval);
	servo2->Set(servoval);
	servo3->Set(servoval);
	servo4->Set(servoval);
	//! @bug printf("float=%f",1.234f);が正常に作動しない
	printf("roll=%5d\n\r",(int)roll);
	printf("pitch=%5d\n\r",(int)pitch);
	fprintf(Xbeefile,"\033[H");
	fprintf(Xbeefile,"Gyro:\n\r");
	fprintf(Xbeefile,"X:%10d,Y:%10d,Z:%10d\n\r",Gyro[0],Gyro[1],Gyro[2]);
	fprintf(Xbeefile,"Acc:\n\r");
	fprintf(Xbeefile,"X:%10d,Y:%10d,Z:%10d\n\r",Acc[0],Acc[1],Acc[2]);
	fprintf(Xbeefile,"Mag:\n\r");
	fprintf(Xbeefile,"X:%10d,Y:%10d,Z:%10d\n\r",Mag[0],Mag[1],Mag[2]);
	fprintf(Xbeefile,"roll=%5d\n\r",(int)roll);
	fprintf(Xbeefile,"pitch=%5d\n\r",(int)pitch);
	printf("Elapsed time%d[ms]\n\r",TimingManager::GetElapsedTIme());
	if(_hardware_errcode!=NoErr){
		printf("Hardware Error:code=%d\n\r",_hardware_errcode);
	}
	colorflag++;
	colorflag&=0x07;
	pos=pos+1.0f;
	if(pos>90.0f){
		pos=0.0f;
	}*/
	ControlExec();
}
/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{ 
  uwTimingDelay = nTime;

  while(uwTimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */

void TimingDelay_Decrement(void)
{
  if (uwTimingDelay != 0x00)
  { 
    uwTimingDelay--;
  }
}
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
