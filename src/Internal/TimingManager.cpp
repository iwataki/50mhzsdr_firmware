/*
 * TimingManager.cpp
 *
 *  Created on: 2014/12/31
 *      Author: ���@�@��Y
 */

#include "TimingManager.h"
#include "DeviceManager.h"
#include <stm32f4xx_conf.h>
#include <stdio.h>
void TimingManager::PendSV_Request(void){
	  SCB->ICSR|=0x10000000;
}
void TimingManager::Exec(void){
	switch(state){
	case Request_DeviceUpdate:
		//printf("TM:UpdateReq\n\r");
		DeviceManager::Update();
		state=WAIT_Update;
		PendSV_Request();
		elapsed_time=0;
		break;
	case WAIT_Update:
		if(elapsed_time>Period/2){
			printf("Device update timeout\n\r");
			state=Device_Timeout;
		}
		elapsed_time++;
		break;
	case Device_Timeout:
		printf("Device update timeout(Never seen)\n\r");
		elapsed_time++;
		break;
	case Exec_Loop:
		if(elapsed_time>Period-1){
			printf("Control Timeout\n\r");
			state=Request_DeviceUpdate;
		}else{
			elapsed_time++;
		}
		break;
	case Sleeping:
		if(elapsed_time>=Period-1){
			state=Request_DeviceUpdate;
		}else{
			elapsed_time++;
		}
		break;
	}
}
void TimingManager::PendSV_Handler(void){
	if(state==WAIT_Update){
		while(DeviceManager::IsUpdated()==false){
			if(state==Device_Timeout){
				break;
			}
		}
		state=Exec_Loop;
	}
	if(state==Exec_Loop){//Call control loop
		//printf("Call Ctrllp\n\r");
		PeriodicFunc();
		state=Sleeping;
	}

}
int TimingManager::GetElapsedTIme(void){
	return elapsed_time;
}
void TimingManager::Init(int ControlPeriod,_loop_func Func){
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);//1ms
	NVIC_SetPriority(SysTick_IRQn,1);
	NVIC_InitTypeDef init;
	init.NVIC_IRQChannel=PendSV_IRQn;
	init.NVIC_IRQChannelCmd=ENABLE;
	init.NVIC_IRQChannelPreemptionPriority=7;
	init.NVIC_IRQChannelSubPriority=1;
	//SCB_Type b;
	//NVIC_Init(&init);
	NVIC_SetPriority(PendSV_IRQn,0x0f);
	PeriodicFunc=Func;
	Period=ControlPeriod;
	//printf("initTM\n\r");
}
void Timingmanager_Exec(void){
	TimingManager::Exec();
}
void Timingmanager_PendSV_Handler(void){
	TimingManager::PendSV_Handler();
}
TimingManager::TimingState TimingManager::state=TimingManager::Sleeping;
int TimingManager::Period=0;
_loop_func TimingManager::PeriodicFunc;
int TimingManager::elapsed_time=0;
